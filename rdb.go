package main

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"github.com/go-redis/redis/v8"
)

var (
	ctx = context.Background()
)

func RdbConn() *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("GOTHENTICATOR_REDIS_ADDRESS"),
		Password: os.Getenv("GOTHENTICATOR_REDIS_PASSWORD"),
		DB:       0,
	})

	return rdb
}

func TestRedisConnection() {
	conn := RdbConn()
	defer conn.Close()

	_, err := conn.Ping(ctx).Result()
	if err != nil {
		log.Fatal("Redis connection error: ", err)
		os.Exit(1)
	}

	log.Println("Redis connection is successful")
}

func SetUser(key string, value interface{}) error {
	conn := RdbConn()
	defer conn.Close()

	err := conn.Set(ctx, key, value, 0).Err()
	if err != nil && err != redis.Nil {
		log.Fatal("FATAL : Error occurred when trying to set a new user into the database\n", err)
		return err
	}

	return nil
}

func GetUser(key string, dest interface{}) error {
	conn := RdbConn()
	defer conn.Close()

	p, err := conn.Get(ctx, key).Result()
	if err != nil && err != redis.Nil {
		log.Fatal("FATAL : Error occurred when trying to get user from the database\n", err)
		return err
	}

	return json.Unmarshal([]byte(p), dest)
}
