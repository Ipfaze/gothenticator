package main

import (
	"encoding/json"
	"time"
)

type User struct {
	Email     string    `json:"email" validate:"required,email"`
	Password  string    `json:"password" validate:"required"`
	Salt      []byte    `json:"salt" validate:"required"`
	CreatedAt time.Time `json:"created_at" validate:"required"`
}

func (u User) String() string {
	userJSON, _ := json.Marshal(u)
	return string(userJSON)
}

func (u User) MarshalBinary() (data []byte, err error) {
	bytes, err := json.Marshal(u)
	return bytes, err
}
