package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
)

var (
	validate = validator.New()
)

type Credentials struct {
	Email           string `json:"email" validate:"required,email"`
	Password        string `json:"password" validate:"required,gte=10"`
	ConfirmPassword string `json:"confirm_password"`
}

func main() {
	TestRedisConnection()

	http.HandleFunc("/signin", Signin)
	http.HandleFunc("/signup", Signup)

	http.ListenAndServe(":8080", nil)
	log.Println("INFO : Service listening on port 8080")
}

func Signin(w http.ResponseWriter, r *http.Request) {
	var credentials Credentials
	var user User

	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		log.Println("DEBUG : An error occurred when trying to decode credentials\n", err)
		respondWithError(w, r, http.StatusBadRequest, "application/json", "invalid request")
		return
	}

	err = validate.Struct(credentials)
	if err != nil {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "please verify your credentials")
		return
	}

	err = GetUser(credentials.Email, &user)
	if err != nil {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "please verify your credentials")
		return
	}

	if !DoPasswordsMatch(user.Password, credentials.Password, user.Salt) {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "please verify your credentials")
		return
	}

	log.Println("INFO : ", user.Email, " is now connected")

	token, _ := CreateJwtToken(user)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(createJsonMessageReturn("token", token))
}

func Signup(w http.ResponseWriter, r *http.Request) {
	var credentials Credentials
	var user User

	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		log.Println("DEBUG : An error occurred when trying to decode credentials\n", err)
		respondWithError(w, r, http.StatusBadRequest, "application/json", "invalid request")
		return
	}

	err = validate.Struct(credentials)
	if err != nil {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "please verify your credentials")
		return
	}

	if credentials.Password != credentials.ConfirmPassword {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "password and confirm password must be the same")
		return
	}

	_ = GetUser(credentials.Email, &user)
	if user.Email != "" {
		respondWithError(w, r, http.StatusBadRequest, "application/json", "user already exists")
		return
	}

	user.Email = credentials.Email
	user.Salt = GenerateRandomSalt()
	user.Password = GenerateHashPassword(credentials.Password, user.Salt)
	user.CreatedAt = time.Now()

	err = validate.Struct(user)
	if err != nil {
		log.Println("ERROR : User information is invalid before insert in database\n", err)
		respondWithError(w, r, http.StatusBadRequest, "application/json", "internal server error")
		return
	}

	err = SetUser(user.Email, user)
	if err != nil {
		log.Println("DEBUG : Failed to set key : ", user.Email, ", value : ", user, "\n", err.Error())
		respondWithError(w, r, http.StatusInternalServerError, "application/json", "internal server error")
		return
	}

	log.Println("INFO : ", user.Email, " has signup")

	token, _ := CreateJwtToken(user)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(createJsonMessageReturn("token", token))
}

func createJsonMessageReturn(key string, message string) []byte {
	resp := make(map[string]string)
	resp[key] = message
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatal("FATAL : Error occurred in JSON marshal. Err: ", err)
	}
	return jsonResp
}

func respondWithError(w http.ResponseWriter, r *http.Request, status int, contentType string, message string) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", contentType)
	w.Write(createJsonMessageReturn("error", message))
}
