package main

import (
	"github.com/dgrijalva/jwt-go/v4"
)

var (
	jwtKey = []byte("YOUR_SECRET")
)

func CreateJwtToken(user User) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["password"] = user.Password

	return token.SignedString(jwtKey)
}
