package main

import (
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"log"
)

const (
	SALT_SIZE = 52
)

func GenerateRandomSalt() []byte {
	var salt = make([]byte, SALT_SIZE)

	_, err := rand.Read(salt[:])

	if err != nil {
		log.Fatal("FATAL : Error occurred when trying to generate salt\n", err)
	}

	return salt
}

func GenerateHashPassword(password string, salt []byte) string {
	sha512Hasher := sha512.New()
	passwordBytes := []byte(password)

	passwordBytes = append(passwordBytes, salt...)

	sha512Hasher.Write(passwordBytes)

	return base64.URLEncoding.EncodeToString(sha512Hasher.Sum(nil))
}

func DoPasswordsMatch(hashedPassword, currPassword string, salt []byte) bool {
	var currPasswordHash = GenerateHashPassword(currPassword, salt)

	return hashedPassword == currPasswordHash
}
